package com.github_project.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

    @SerializedName("items")
    private List<Item>items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
