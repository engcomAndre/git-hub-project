package com.github_project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github_project.R;
import com.github_project.infra.api.RepositorieData;
import com.github_project.viewholder.GitHubUserRepoViewHolder;

import java.util.ArrayList;
import java.util.List;

public class GitRepoAdapter extends RecyclerView.Adapter<GitHubUserRepoViewHolder> {

    private Context mContext;
    private List<RepositorieData> repoList = new ArrayList<>();

    public GitRepoAdapter(Context context, List<RepositorieData> repoNameList) {
        this.mContext = context;
        this.repoList = repoNameList;
    }

    @NonNull
    @Override
    public GitHubUserRepoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(this.mContext);
        View view = inflater.inflate(R.layout.git_repo_data, parent, false);
        return new GitHubUserRepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GitHubUserRepoViewHolder holder, int position) {
        RepositorieData repoData = this.repoList.get(position);
        holder.bindData(repoData);
    }

    @Override
    public int getItemCount() {
        if(repoList != null){
            return repoList.size();
        }
        return 0;
    }
}
