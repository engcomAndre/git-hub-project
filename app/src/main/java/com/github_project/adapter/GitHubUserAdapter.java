package com.github_project.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github_project.R;
import com.github_project.entities.GitHubUserEntity;
import com.github_project.infra.api.RetrofitGitUser;
import com.github_project.listener.RetrofitGitUserInteractionListener;
import com.github_project.viewholder.GitHubUserViewHolder;

import java.util.List;

import retrofit2.Response;

public class GitHubUserAdapter extends RecyclerView.Adapter<GitHubUserViewHolder> {


    private Context mContext;
    private List<RetrofitGitUser> gitHubUserEntities;
    private RetrofitGitUserInteractionListener listener;


    public GitHubUserAdapter(Context mContext, List<RetrofitGitUser> gitHubUserEntities, RetrofitGitUserInteractionListener retrofitGitUserInteractionListener) {
        this.mContext = mContext;
        this.gitHubUserEntities = gitHubUserEntities;
        this.listener = retrofitGitUserInteractionListener;
    }

    @NonNull
    @Override
    public GitHubUserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        this.mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(this.mContext);
        View view = inflater.inflate(R.layout.row_gituser, viewGroup, false);
        return new GitHubUserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GitHubUserViewHolder holder, int position) {
        RetrofitGitUser gitHubUserEntity = this.gitHubUserEntities.get(position);
        holder.bindData(gitHubUserEntity,listener);
    }

    @Override
    public int getItemCount() {
        if(this.gitHubUserEntities != null){
            return this.gitHubUserEntities.size();
        }
        return 0;
    }
}
