package com.github_project.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github_project.R;
import com.github_project.adapter.GitHubUserAdapter;
import com.github_project.infra.api.RetrofitClientInstance;
import com.github_project.infra.api.RetrofitService;
import com.github_project.infra.api.RetrofitGitUser;
import com.github_project.listener.RetrofitGitUserInteractionListener;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;


public class MainActivity extends AppCompatActivity {

    private ViewHolder viewHolder = new ViewHolder();
    private RetrofitService.GetDataService service;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mContext = this;
        this.service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitService.GetDataService.class);

        this.viewHolder.relativeLayout = findViewById(R.id.relative_layout);
        this.viewHolder.relativeLayoutProgress = findViewById(R.id.relative_layout_progress);
        this.viewHolder.progressBar = findViewById(R.id.progressBar);

        this.viewHolder.relativeLayoutProgress = findViewById(R.id.relative_layout_progress);
        this.viewHolder.relativeLayoutProgress.setAlpha(1f);
        this.viewHolder.relativeLayoutProgress.setVisibility(GONE);


        this.viewHolder.btn_search = findViewById(R.id.btn_search);
        this.viewHolder.btn_search.setOnClickListener(this.clickListener());

        this.viewHolder.editSearch = findViewById(R.id.input_search);

        this.viewHolder.gitUsersRecycler = findViewById(R.id.git_hub_users_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        this.viewHolder.gitUsersRecycler.setLayoutManager(manager);
        this.viewHolder.gitUsersRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        waitingAnimationON();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Call<List<RetrofitGitUser>> call = service.getUsers();
        call.enqueue(this.getCallBackUsers());
    }

    private RetrofitGitUserInteractionListener retrofitGitUserInteractionListener(){
        return new RetrofitGitUserInteractionListener() {
            @Override
            public void onListClick(RetrofitGitUser retrofitGitUser) {
                Call<RetrofitGitUser> call = service.getUserData(retrofitGitUser.getLogin());
                call.enqueue(getCallBackUserByLogin());
            }
        };
    }



    public Callback<List<RetrofitGitUser>> getCallBackUsers(){
        return new Callback<List<RetrofitGitUser>>() {
            @Override
            public void onResponse(Call<List<RetrofitGitUser>> call, Response<List<RetrofitGitUser>> response) {
                GitHubUserAdapter gitHubUserAdapter = new GitHubUserAdapter(mContext,response.body(),retrofitGitUserInteractionListener());
                viewHolder.gitUsersRecycler.setAdapter(gitHubUserAdapter);
                waitingAnimationOFF();
            }

            @Override
            public void onFailure(Call<List<RetrofitGitUser>> call, Throwable t) {
                waitingAnimationOFF();
                Toast.makeText(mContext,"Um erro ocorreu...",Toast.LENGTH_LONG).show();
            }
        };

    }

    private View.OnClickListener clickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchLogin = viewHolder.editSearch.getText().toString();
                if(!searchLogin.isEmpty()){
                    Call<RetrofitGitUser> call = service.getUserData(searchLogin);
                    call.enqueue(getCallBackUserByLogin());
                }
                else {
                    Toast.makeText(mContext,"Insira um login válido!",Toast.LENGTH_LONG).show();
                }
            }
        };
    }
    public Callback<RetrofitGitUser> getCallBackUserByLogin(){
        return new Callback<RetrofitGitUser>() {
            @Override
            public void onResponse(Call<RetrofitGitUser> call, Response<RetrofitGitUser> response) {
                gotoInfoUserActivitie(response.body());
                waitingAnimationOFF();
            }

            @Override
            public void onFailure(Call<RetrofitGitUser> call, Throwable t) {
                waitingAnimationOFF();
                Toast.makeText(mContext,"Um erro ocorreu...",Toast.LENGTH_LONG).show();
            }
        };
    }

    private void gotoInfoUserActivitie(RetrofitGitUser body) {
        Intent intent = new Intent(this.mContext, GitUserDetails.class);
        intent.putExtra("obj",body);
        startActivity(intent);
    }

    private void waitingAnimationON() {
        this.viewHolder.relativeLayout.setAlpha(0.5f);
        this.viewHolder.relativeLayoutProgress.setAlpha(1f);
        this.viewHolder.relativeLayout.setVisibility(View.VISIBLE);
        this.viewHolder.relativeLayoutProgress.setVisibility(View.VISIBLE);
    }


    private void waitingAnimationOFF() {
        this.viewHolder.relativeLayout.setAlpha(1f);
        this.viewHolder.relativeLayout.setVisibility(View.INVISIBLE);
        this.viewHolder.relativeLayoutProgress.setVisibility(View.INVISIBLE);
    }

    private static class ViewHolder {
        private TextInputEditText editSearch;

        private RecyclerView gitUsersRecycler;

        private ProgressBar progressBar;
        private RelativeLayout relativeLayout;
        private RelativeLayout relativeLayoutProgress;

        private MaterialButton btn_search;

    }
}


