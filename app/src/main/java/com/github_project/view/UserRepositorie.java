package com.github_project.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.github_project.R;
import com.github_project.adapter.GitRepoAdapter;
import com.github_project.infra.api.RetrofitGitUser;

public class UserRepositorie extends AppCompatActivity {
    private ViewHolder viewHolder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);

        this.viewHolder.repo_list = findViewById(R.id.repo_list);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        this.viewHolder.repo_list.setLayoutManager(manager);
        this.viewHolder.repo_list.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        RetrofitGitUser retrofitGitUser = (RetrofitGitUser) getIntent().getSerializableExtra("obj");

//        GitRepoAdapter gitRepoAdapter = new GitRepoAdapter();
//        this.viewHolder.repo_list.setAdapter(gitRepoAdapter);

        String re = "";

    }


    public static class ViewHolder {
        public RecyclerView repo_list;

    }
}
