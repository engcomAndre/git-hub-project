package com.github_project.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github_project.R;
import com.github_project.adapter.GitRepoAdapter;
import com.github_project.infra.api.RepositorieData;
import com.github_project.infra.api.RetrofitClientInstance;
import com.github_project.infra.api.RetrofitGitUser;
import com.github_project.infra.api.RetrofitService;
import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GitUserDetails extends AppCompatActivity {

    private ViewHolder viewHolder = new ViewHolder();

    private Context mContext;
    private RetrofitGitUser retrofitGitUser;
    private RetrofitService.GetDataService service;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_git_user_details);

        this.mContext = this;
        this.service = RetrofitClientInstance.getRetrofitInstance().create(RetrofitService.GetDataService.class);

        this.viewHolder.gitUserCompany = findViewById(R.id.git_detail_company);
        this.viewHolder.gitUserImg = findViewById(R.id.git_user_img);
        this.viewHolder.gitUserName = findViewById(R.id.git_detail_name);
        this.viewHolder.gitUserUrl = findViewById(R.id.git_detail_url);
        this.viewHolder.getGitUserLocal = findViewById(R.id.git_detail_local);

        this.viewHolder.relativeLayout = findViewById(R.id.relative_layout);
        this.viewHolder.relativeLayoutProgress = findViewById(R.id.relative_layout_progress);
        this.viewHolder.progressBar = findViewById(R.id.progressBar);


        this.viewHolder.btnAltInfo = findViewById(R.id.btn_alt_info);
        this.viewHolder.btnAltInfo.setOnClickListener(this.getClickListener());

        this.retrofitGitUser = (RetrofitGitUser) getIntent().getSerializableExtra("obj");

        this.viewHolder.repoData = findViewById(R.id.git_user_repos_recycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        this.viewHolder.repoData.setLayoutManager(manager);
        this.viewHolder.repoData.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        this.viewHolder.infoRespositories = findViewById(R.id.gituser_repos);
        this.viewHolder.infoGitUser = findViewById(R.id.git_user_info);

        setData();
        waitingAnimationON();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Call<List<RepositorieData>> call = service.getUserRepos(this.retrofitGitUser.getLogin());
        call.enqueue(getListRepoCallback());

    }

    private Callback<List<RepositorieData>> getListRepoCallback(){
        return new Callback<List<RepositorieData>>() {
            @Override
            public void onResponse(Call<List<RepositorieData>> call, Response<List<RepositorieData>> response) {
                String res = "";
                GitRepoAdapter gitRepoAdapter = new GitRepoAdapter(mContext,response.body());
                viewHolder.repoData.setAdapter(gitRepoAdapter);
                waitingAnimationOFF();
            }

            @Override
            public void onFailure(Call<List<RepositorieData>> call, Throwable t) {
                waitingAnimationOFF();
                Toast.makeText(mContext,"Um erro ocorreu,tente novamente mais tarde!",Toast.LENGTH_LONG).show();

            }
        };
    }

    private void setData() {

        Picasso.get()
                .load(this.retrofitGitUser.getImage_url())
                .resize(this.viewHolder.gitUserImg.getLayoutParams().width, this.viewHolder.gitUserImg.getLayoutParams().height)
                .noFade()
                .into(this.viewHolder.gitUserImg);

        this.viewHolder.gitUserName.setText(this.retrofitGitUser.getName());
        this.viewHolder.gitUserUrl.setText(this.retrofitGitUser.getUrl());
        this.viewHolder.gitUserCompany.setText(this.retrofitGitUser.getCompany());
        this.viewHolder.getGitUserLocal.setText(this.retrofitGitUser.getLocation());
        this.viewHolder.btnAltInfo.setText("Mostrar Informações do Usuário");
        waitingAnimationOFF();
    }


    private View.OnClickListener getClickListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.infoRespositories.getVisibility() == View.VISIBLE){
                    viewHolder.infoRespositories.setVisibility(View.GONE);
                    viewHolder.infoGitUser.setVisibility(View.VISIBLE);
                    viewHolder.btnAltInfo.setText("Mostrar Repositórios do Usuário");
                }
                else {
                    viewHolder.infoRespositories.setVisibility(View.VISIBLE);
                    viewHolder.infoGitUser.setVisibility(View.GONE);
                    viewHolder.btnAltInfo.setText("Mostrar Infomações do Usuário");
                }
            }
        };
    }

    private void waitingAnimationON() {
        this.viewHolder.relativeLayout.setAlpha(0.5f);
        this.viewHolder.relativeLayoutProgress.setAlpha(1f);
        this.viewHolder.relativeLayout.setVisibility(View.VISIBLE);
        this.viewHolder.relativeLayoutProgress.setVisibility(View.VISIBLE);
    }


    private void waitingAnimationOFF() {
        this.viewHolder.relativeLayout.setAlpha(1f);
        this.viewHolder.relativeLayout.setVisibility(View.INVISIBLE);
        this.viewHolder.relativeLayoutProgress.setVisibility(View.INVISIBLE);
    }

    private static class ViewHolder {
        private ImageView gitUserImg;
        private TextView gitUserName;
        private TextView gitUserUrl;
        private TextView gitUserCompany;
        private TextView getGitUserLocal;


        private ConstraintLayout infoGitUser;
        private ConstraintLayout infoRespositories;
        private MaterialButton btnAltInfo;

        private RecyclerView repoData;

        private ProgressBar progressBar;
        private RelativeLayout relativeLayout;
        private RelativeLayout relativeLayoutProgress;


    }
}
