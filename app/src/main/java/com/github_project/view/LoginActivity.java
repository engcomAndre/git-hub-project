package com.github_project.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.github_project.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class LoginActivity extends AppCompatActivity {

    private ViewHolder viewHolder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.viewHolder.layoutEmail = findViewById(R.id.layout_email);
        this.viewHolder.layoutPassword = findViewById(R.id.layout_password);
        this.viewHolder.inputPassword = findViewById(R.id.input_email);
        this.viewHolder.inputPassword = findViewById(R.id.input_password);
        this.viewHolder.btnLogin = findViewById(R.id.btn_login);

        this.viewHolder.btnLogin.setOnClickListener(this.getClickListener());

        this.viewHolder.layoutEmail.setError("Alguma coisa não está certa no email...");
    }

    public View.OnClickListener getClickListener() {
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainActivity();
            }
        };

        return clickListener;
    }

    private void goToMainActivity(){
        startActivity(new Intent(this,MainActivity.class));
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();
    }

    private static class ViewHolder{
        private TextInputLayout layoutEmail;
        private TextInputLayout layoutPassword;
        private TextInputEditText inputEmail;
        private TextInputEditText inputPassword;
        private MaterialButton btnLogin;
    }
}
