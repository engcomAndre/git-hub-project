package com.github_project.utils;


import android.content.Context;
import android.net.ConnectivityManager;

public class Connection {

    public static boolean verificaConexao(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connectivityManager != null ) {
            connectivityManager.getActiveNetworkInfo();
            //Verifica internet pela WIFI
            if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()) {
                return true;
            }
            //Verifica se tem internet móvel
            return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
        }
        return false;
    }
}