package com.github_project.viewholder;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github_project.R;
import com.github_project.infra.api.RetrofitGitUser;
import com.github_project.listener.RetrofitGitUserInteractionListener;
import com.squareup.picasso.Picasso;

public class GitHubUserViewHolder extends RecyclerView.ViewHolder {

    private ImageView imageView;
    private TextView login;
    private TextView url_html;
    private LinearLayout linearRowUser;

    public GitHubUserViewHolder(@NonNull View itemView) {
        super(itemView);

        this.imageView = itemView.findViewById(R.id.github_img);
        this.login = itemView.findViewById(R.id.login_user);
        this.url_html = itemView.findViewById(R.id.url_html);
        this.linearRowUser = itemView.findViewById(R.id.layout_row_user);

    }

    public void bindData(final RetrofitGitUser gitHubUserEntity, final RetrofitGitUserInteractionListener listener){
        Picasso.get()
                .load(gitHubUserEntity.getImage_url())
                .resize(imageView.getLayoutParams().width,imageView.getLayoutParams().height).noFade().into(imageView);

        this.login.setText(gitHubUserEntity.getName());
        this.url_html.setText(gitHubUserEntity.getUrl());

        this.linearRowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onListClick(gitHubUserEntity);
            }
        });
    }
}
