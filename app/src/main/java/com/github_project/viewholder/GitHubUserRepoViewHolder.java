package com.github_project.viewholder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github_project.R;
import com.github_project.infra.api.RepositorieData;

public class GitHubUserRepoViewHolder extends RecyclerView.ViewHolder {

    private TextView repoName;
    private TextView repoURL;


    public GitHubUserRepoViewHolder(@NonNull View itemView) {
        super(itemView);
        this.repoName = itemView.findViewById(R.id.repo_data_name);
        this.repoURL = itemView.findViewById(R.id.repo_data_url);

    }

    public void bindData(RepositorieData repo) {
        this.repoName.setText(repo.getName());
        this.repoURL.setText(repo.getHtml_url());
    }
}
