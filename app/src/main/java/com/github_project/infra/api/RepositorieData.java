package com.github_project.infra.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RepositorieData implements Serializable {

    @SerializedName("name")
    private String name;

    @SerializedName("html_url")
    private String html_url;

    public RepositorieData(String name, String html_url) {
        this.name = name;
        this.html_url = html_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }
}
