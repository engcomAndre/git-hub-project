package com.github_project.infra.api;



import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface RetrofitService {

    interface GetDataService {

        @GET("/users")
        Call<List<RetrofitGitUser>> getUsers();

        @GET("/users/{user}")
        Call<RetrofitGitUser> getUserData(@Path("user") String user);

        @GET("/users/{user}/repos")
        Call<List<RepositorieData>> getUserRepos(@Path("user") String user);
    }
}
