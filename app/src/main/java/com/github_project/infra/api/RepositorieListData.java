package com.github_project.infra.api;

import java.io.Serializable;
import java.util.List;

public class RepositorieListData implements Serializable {

    private List<RepositorieData> repositorieData;

    public RepositorieListData(List<RepositorieData> repositorieData) {
        this.repositorieData = repositorieData;
    }


    public List<RepositorieData> getRepositorieData() {
        return repositorieData;
    }

    public void setRepositorieData(List<RepositorieData> repositorieData) {
        this.repositorieData = repositorieData;
    }
}
