package com.github_project.listener;

import com.github_project.infra.api.RetrofitGitUser;

public interface RetrofitGitUserInteractionListener {
    void onListClick(RetrofitGitUser retrofitGitUser);
}
