package com.github_project;


import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.github_project.view.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.rule.ActivityTestRule;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4.class)
public class SplashScreenTest {


    @Rule
    public ActivityTestRule<SplashActivity>
            mActivityRule = new ActivityTestRule<>(SplashActivity.class, false, true);

    @Test
    public void whenActivityIsLaunched_shouldDisplayInitialState_in_production() {
        onView(withId(R.id.logo_git)).check(matches((isDisplayed())));
    }
}


